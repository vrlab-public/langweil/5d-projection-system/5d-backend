FROM ubuntu:eoan

RUN apt-get update && \
    apt-get install -y openjdk-14-jdk && \
    apt-get clean;
ENV JAVA_VER 14
ENV JAVA_HOME /usr/lib/jvm/java-14-openjdk-amd64

RUN apt-get install -y ffmpeg net-tools && \
    apt-get clean;

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080




RUN echo $JAVA_HOME

#ENTRYPOINT ["java","-jar","/app.jar"]
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=dock","/app.jar"]
