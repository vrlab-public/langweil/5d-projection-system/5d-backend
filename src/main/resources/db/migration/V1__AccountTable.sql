DROP TABLE IF EXISTS account;
-- CREATE EXTENSION "uuid-ossp";

CREATE TABLE client
(
    id BIGSERIAL NOT NULL PRIMARY KEY
);


CREATE TABLE account
(
    id         BIGSERIAL                NOT NULL PRIMARY KEY,
    username   VARCHAR(100)             NOT NULL UNIQUE,
    password   VARCHAR(100)             NOT NULL,
    roles      VARCHAR(100),
    createdCet TIMESTAMP WITH TIME ZONE NOT NULL,
--     createdBy  BIGSERIAL,
    active     BOOLEAN                  NOT NULL
);

CREATE TABLE trackType
(
    id   BIGSERIAL    NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

INSERT INTO trackType (name)
VALUES ('PICTURE');
INSERT INTO trackType (name)
VALUES ('VIDEO');
INSERT INTO trackType (name)
VALUES ('AUDIO');

CREATE TABLE displaySet
(
    id          BIGSERIAL                      NOT NULL PRIMARY KEY,
    createdBy   BIGINT REFERENCES account (id) NOT NULL,
    name        VARCHAR(100)                   NOT NULL,
    description VARCHAR(300),
    createdCet  TIMESTAMP WITH TIME ZONE       NOT NULL,
    duration    FLOAT                          NOT NULL,
    published   BOOLEAN                        NOT NULL,
    changed     BOOLEAN                        NOT NULL,
    autonomous  INT                            NOT NULL
);

CREATE TABLE displayTrack
(
    id           BIGSERIAL                         NOT NULL PRIMARY KEY,
    displaySetId BIGINT REFERENCES displaySet (id) NOT NULL,
    name         VARCHAR(100)                      NOT NULL,
    trackType    VARCHAR(300)                      NOT NULL,
    duration     FLOAT                             NOT NULL,
    createdBy    BIGINT REFERENCES account (id)    NOT NULL,
    createdCet   TIMESTAMP WITH TIME ZONE          NOT NULL,
    description  VARCHAR(300)
);
-- Folder
CREATE TABLE folder
(
    id             BIGSERIAL    NOT NULL PRIMARY KEY,
    parentFolderId BIGINT REFERENCES folder (id),
    path           VARCHAR(300) NOT NULL
);

INSERT INTO folder (path)
VALUES ('/');
-- INSERT INTO folder (path, parentFolderId)
-- VALUES ('start/', 1);


CREATE TABLE videoFile
(
    videoFileId        BIGSERIAL                        NOT NULL PRIMARY KEY,
    createdBy          BIGINT REFERENCES account (id)   NOT NULL,
    createdCet         TIMESTAMP WITH TIME ZONE         NOT NULL,
    fileName           VARCHAR(100)                     NOT NULL,
    filePath           VARCHAR(300)                     NOT NULL,
    folderId           BIGINT                           NOT NULL,
    clientSynchronized BOOLEAN                          NOT NULL,
    duration           FLOAT                            NOT NULL,
    fileSize           BIGINT                           NOT NULL,
    videoFileType      BIGINT REFERENCES trackType (id) NOT NULL,
    description        VARCHAR(300)
);


CREATE TABLE displayTrackToVideoFile
(
    displayTrackToVideoFileId BIGSERIAL                                 NOT NULL PRIMARY KEY,
    displayTrackId            BIGINT REFERENCES displayTrack (id)       NOT NULL,
    client                    BIGINT REFERENCES client (id)             NOT NULL,
    videoFileId               BIGINT REFERENCES videoFile (videoFileId) NOT NULL,
    createdBy                 BIGINT REFERENCES account (id)            NOT NULL,
    createdCet                TIMESTAMP WITH TIME ZONE                  NOT NULL
);

CREATE TABLE logs
(
    message   TEXT                     NOT NULL,
    timestamp TIMESTAMP WITH TIME ZONE NOT NULL
);

-- Clients
INSERT INTO client (id)
VALUES (DEFAULT);
INSERT INTO client (id)
VALUES (DEFAULT);
INSERT INTO client (id)
VALUES (DEFAULT);
INSERT INTO client (id)
VALUES (DEFAULT);
INSERT INTO client (id)
VALUES (DEFAULT);

-- Accounts
INSERT INTO account (id, username, password, roles, createdCet, active)
VALUES (DEFAULT, 'admin', '$2a$10$2DAipSvgd75ir6BZ3a7NiOHRwi5sEWr9AP5yDO65034aZjZnc2f8e', 'ADMIN,PRESENTATOR',
        current_timestamp, TRUE);

-- Display set
-- INSERT INTO displaySet (createdBy, createdCet, name, published, autonomous, duration, changed)
-- VALUES (1, current_timestamp, 'start', false, true, 4.079999923706055, true);


INSERT INTO account (username, password, roles, createdCet, active)
VALUES ('user1', '$2a$10$2DAipSvgd75ir6BZ3a7NiOHRwi5sEWr9AP5yDO65034aZjZnc2f8e', 'PRESENTATOR', current_timestamp,
        TRUE);
INSERT INTO account (username, password, roles, createdCet, active)
VALUES ('user2', '$2a$10$2DAipSvgd75ir6BZ3a7NiOHRwi5sEWr9AP5yDO65034aZjZnc2f8e',
        'ADMIN,PRESENTATOR', current_timestamp, TRUE);


-- Video tracks
-- INSERT INTO displayTrack (displaySetId, createdBy, createdCet, name, trackType, duration)
-- VALUES (1, 1, current_timestamp, 'start_loop', 'VIDEO', 4.079999923706055);


-- Video files
-- INSERT INTO videoFile (createdBy, createdCet, fileName, filePath, clientSynchronized, duration, fileSize, folderId,
--                        videoFileType)
-- VALUES (1, current_timestamp, 'loop.mp4', '/home/otrojan/vid/start/loop.mp4', TRUE, 4.079999923706055, 201962, 1, 2);


-- Video file - video track relationship
-- INSERT INTO displayTrackToVideoFile (displayTrackId, videoFileId, createdBy, createdCet, client)
-- VALUES (1, 1, 1, current_timestamp, 1);
-- INSERT INTO displayTrackToVideoFile (displayTrackId, videoFileId, createdBy, createdCet, client)
-- VALUES (1, 1, 1, current_timestamp, 2);


-- Log
INSERT INTO logs (message, timestamp)
VALUES ('Test log', current_timestamp);