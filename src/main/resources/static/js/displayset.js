import {serverUrl, getCookie, sendHttpRequest} from './common.js';

let displaySetDetailContent = document.querySelector("div#videoSetDetail")
let videoSetPublishButtons = document.querySelectorAll(".videoset-publish")
let videoSetDetailButtons = document.querySelectorAll(".video-set-detail")
let formElement = document.querySelector(".sync-form")
videoSetPublishButtons.forEach(button => button.addEventListener('click', function () {
    let publishedField = document.querySelector("span#field-publish-" + this.value.toString());
    publishedField.innerText = "";
    publishedField.classList.add("spinner-border");
    sendHttpRequest('POST', serverUrl + '/control/videoedit/videoset/publish/' + this.value)
        .then(responseData => {
            location.reload();
        }).catch(err => {
    })
}));

videoSetDetailButtons.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('GET', serverUrl + '/control/videoedit/videoset/' + this.value)
        .then(responseData => {
            clearDetailContainer();
            displaySetDetailContent.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {
    })
}));
formElement.addEventListener("submit", function () {
    document.querySelector("div#spinnerPosition").classList.add("spinner-border");
});

function clearDetailContainer() {
    displaySetDetailContent = document.querySelector("div#videoSetDetail")
    if (displaySetDetailContent) displaySetDetailContent.innerHTML = ""
}