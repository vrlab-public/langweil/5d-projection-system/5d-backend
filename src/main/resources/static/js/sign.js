//
//
var form = document.querySelector('.form-signin');
var token
var serverUrl = 'http://localhost:8080';
var alertbox = document.querySelector('.alert-message-box');

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}


const sendHttpRequest = (method, url, data) => {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url)
        console.log("data " + data)
        if (data) {
            xhr.setRequestHeader('Content-Type', 'application/json');
        }
        xhr.onload = () => {
            if (xhr.status > 400) {
                reject(xhr.response)
            } else {
                resolve(xhr)
            }
        };
        xhr.onerror = () => {
            reject('Something went wrong check the backend log')
        };

        xhr.send(JSON.stringify(data));
    });
    return promise;

}
const formLogin = (event) => {
    alertbox.innerHTML = '';
    console.log(event)
    sendHttpRequest('POST', serverUrl + '/login', {
        "username": document.querySelector('#username').value,
        "password": document.querySelector('#password').value
    }).then(xhr => {
        alertbox.innerHTML = '';
        token = xhr.getResponseHeader("Authorization");
        document.cookie = "Authorization=" + token;
        window.location.replace(serverUrl);
    }).catch(err => {
        alertbox.insertAdjacentHTML('beforeend', '<div class="alert alert-danger" role="alert">Wrong username or password</div>');
        console.log(err);
    })
    event.preventDefault();
}

form.addEventListener("submit", formLogin);




