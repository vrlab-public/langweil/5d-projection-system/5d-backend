import {serverUrl, getCookie, sendHttpRequest} from './common.js';

let displayTrackDetailContent = document.querySelector("div#videoTrackDetail")
let displayTrackAddContent = document.querySelector("div#createVideoTrack")
let videoTrackDetailsButtons = document.querySelectorAll(".video-track-detail")
videoTrackDetailsButtons.forEach(button => button.addEventListener('click', function () {
    sendHttpRequest('GET', serverUrl + '/control/videoedit/videotrack/' + this.value)
        .then(responseData => {
            displayTrackAddContent = document.querySelector("div#createVideoTrack")
            displayTrackAddContent.innerHTML = "";
            clearDetailContainer();
            displayTrackDetailContent.insertAdjacentHTML('beforeend', responseData);
        }).catch(err => {
        })
}));


function clearDetailContainer() {
    displayTrackDetailContent = document.querySelector("div#videoTrackDetail")
    if (displayTrackDetailContent) displayTrackDetailContent.innerHTML = ""
}