package com.museum.projection.exception;

/**
 * StorageFile NotFound Exception
 */
public class StorageFileNotFoundException extends Throwable {
}
