package com.museum.projection.util.validation.validators;

import com.museum.projection.util.validation.OnlyFilename;
import com.museum.projection.util.validation.OnlyText;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator
 */
public class FilenameValidator implements ConstraintValidator<OnlyFilename, String> {


    @Override
    public void initialize(OnlyFilename constraintAnnotation) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = s != null && s.matches("[a-zA-Z_0-9]*.[a-zA-Z0-9]*");

        if (!isValid) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(
                    "filename must be in format filename.mp4  or filename_1.avi (Alphanumerical with backslash and dot)"
            )
                    .addConstraintViolation();
        }
        return isValid;
    }

    private String string;


}