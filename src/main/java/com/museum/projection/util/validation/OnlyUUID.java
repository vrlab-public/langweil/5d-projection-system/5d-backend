package com.museum.projection.util.validation;

import com.museum.projection.util.validation.validators.OnlyUUIDValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.NotNull;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE ,CONSTRUCTOR, TYPE_USE})
@Retention(RUNTIME)

@Constraint(validatedBy = OnlyUUIDValidator.class)
@Documented
public @interface OnlyUUID {

    String message() default "input is not a valid UUID";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

}

