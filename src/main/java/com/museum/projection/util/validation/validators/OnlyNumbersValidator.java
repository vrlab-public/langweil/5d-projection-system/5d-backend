package com.museum.projection.util.validation.validators;

import com.museum.projection.util.validation.OnlyNumbers;
import com.museum.projection.util.validation.OnlyUUID;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validator
 */
public class OnlyNumbersValidator implements ConstraintValidator<OnlyNumbers, String> {

    private String message;

    @Override
    public void initialize(OnlyNumbers constraintAnnotation) {
        message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean isValid = s != null && s.matches("\\d+");

        if (!isValid) {
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate(this.message)
                    .addConstraintViolation();
        }
        return isValid;
    }

    private String string;


}