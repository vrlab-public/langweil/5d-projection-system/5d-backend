package com.museum.projection.util;

import org.apache.commons.io.FilenameUtils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

/**
 * String utils for centralized String conversions
 */
public class StringUtils {
    final static String TIME_PATTERN = "HH:mm:ss";
    final static String DATE_PATTERN = "dd. MM. yyyy HH:mm:ss";
    public static SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
    public static SimpleDateFormat timeFormat = new SimpleDateFormat(TIME_PATTERN);

    public static String toDateTime(Timestamp timestamp) {
        return dateFormat.format(Date.from(timestamp.toInstant()));
    }



    /**
     * Conversion to readable size  in byes
     * @param bytes
     * @return
     */
    //    credits https://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format-in-java
    public static String humanReadableByteCountSI(long bytes) {
        String s = bytes < 0 ? "-" : "";
        long b = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        return b < 1000L ? bytes + " B"
                : b < 999_950L ? String.format("%s%.1f kB", s, b / 1e3)
                : (b /= 1000) < 999_950L ? String.format("%s%.1f MB", s, b / 1e3)
                : (b /= 1000) < 999_950L ? String.format("%s%.1f GB", s, b / 1e3)
                : (b /= 1000) < 999_950L ? String.format("%s%.1f TB", s, b / 1e3)
                : (b /= 1000) < 999_950L ? String.format("%s%.1f PB", s, b / 1e3)
                : String.format("%s%.1f EB", s, b / 1e6);
    }

    /**
     * Conversion to readable size in bits
     * @param bytes
     * @return
     */
    public static String humanReadableByteCountBin(long bytes) {
        long b = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        return b < 1024L ? bytes + " B"
                : b <= 0xfffccccccccccccL >> 40 ? String.format("%.1f KiB", bytes / 0x1p10)
                : b <= 0xfffccccccccccccL >> 30 ? String.format("%.1f MiB", bytes / 0x1p20)
                : b <= 0xfffccccccccccccL >> 20 ? String.format("%.1f GiB", bytes / 0x1p30)
                : b <= 0xfffccccccccccccL >> 10 ? String.format("%.1f TiB", bytes / 0x1p40)
                : b <= 0xfffccccccccccccL ? String.format("%.1f PiB", (bytes >> 10) / 0x1p40)
                : String.format("%.1f EiB", (bytes >> 20) / 0x1p40);
    }

    /**
     * Trim last slash in string
     * @param path
     * @return
     */
    public static String cutLastSlash(String path) {
        if (path.length() > 1 && path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }

    /**
     * @param fileName
     * @return
     */
    public static String removeFileTypeExtension(String fileName) {
        return FilenameUtils.removeExtension(fileName);
    }

    /**
     * @param seconds
     * @return
     */
    public static  String getTimeFromSeconds(int seconds) {

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        return twoDigitString(minutes) + ":" + twoDigitString(seconds);
    }

    private static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

}
