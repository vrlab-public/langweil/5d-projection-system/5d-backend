package com.museum.projection.config;

import com.museum.projection.clients.DummyClient;
import com.museum.projection.clients.RemoteClient;
import com.museum.projection.clients.VlcClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.ArrayList;
import java.util.List;

/**
 * Configuration of remote clients
 * VLC or Dummy
 */
@Configuration
@EnableScheduling
public class RemoteClientsConfig {

    private final CustomConfig propertiesConfig;

    @Autowired
    public RemoteClientsConfig(CustomConfig propertiesConfig) {
        this.propertiesConfig = propertiesConfig;
    }

    /**
     * If scheduling enable register VLC client beans
     * @return
     */
    @Bean("clients")
    @ConditionalOnProperty(name = "5d.scheduling.enabled", havingValue = "true", matchIfMissing = true)
    public List<RemoteClient> Clients() {
        List<RemoteClient> clients = new ArrayList<>();
        for (int i = 0; i < propertiesConfig.getClientCount(); i++) {
            System.out.println("Creating bean for vlcClient " + i);
            clients.add(new VlcClient(propertiesConfig.getIp(i), propertiesConfig.getVlcPort(i), i, propertiesConfig.getVlcVersionCheck()));
        }
        return clients;
    }

    /**
     * If scheduling not enabled register Dummy client beans
     * @return
     */
    @Bean("clients")
    @ConditionalOnProperty(name = "5d.scheduling.enabled", havingValue = "false")
    public List<RemoteClient> ClientsDummy() {
        List<RemoteClient> clients = new ArrayList<>();
        for (int i = 0; i < propertiesConfig.getClientCount(); i++) {
            clients.add(new DummyClient(i));
        }
        return clients;
    }
}


