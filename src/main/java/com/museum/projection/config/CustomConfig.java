package com.museum.projection.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom configuration object
 */
@Component
@ConfigurationProperties(prefix = "5d")
public class CustomConfig {

    private String playlistFolderPath;
    private String videoFolderPath;
    private Boolean vlcVersionCheck;
    private Boolean queryAll;
    private List<String> clientIps = new ArrayList<>();
    private List<Integer> clientVlcPorts = new ArrayList<>();
    private List<Integer> clientSshPorts = new ArrayList<>();
    private List<String> clientUsername = new ArrayList<>();
    private List<String> clientPasswords = new ArrayList<>();

    private boolean enableVlcScheduling = true;
    private int clientCount;


    public String getVideoFolderPath() {
        return videoFolderPath;
    }

    public void setVideoFolderPath(String videoFolderPath) {
        this.videoFolderPath = videoFolderPath;
    }

    public boolean isEnableVlcScheduling() {
        return enableVlcScheduling;
    }

    public void setEnableVlcScheduling(boolean enableVlcScheduling) {
        this.enableVlcScheduling = enableVlcScheduling;
    }

    public int getClientCount() {
        return clientCount;
    }

    public void setClientCount(int clientCount) {
        this.clientCount = clientCount;
    }

    public String getPlaylistFolderPath() {
        return playlistFolderPath;
    }

    public void setPlaylistFolderPath(String playlistFolderPath) {
        this.playlistFolderPath = playlistFolderPath;
    }

    public List<String> getClientIps() {
        return clientIps;
    }

    public void setClientIps(List<String> clientIps) {
        this.clientIps = clientIps;
    }

    public List<Integer> getClientVlcPorts() {
        return clientVlcPorts;
    }

    public void setClientVlcPorts(List<Integer> clientVlcPorts) {
        this.clientVlcPorts = clientVlcPorts;
    }

    public String getIp(int client) {
        return clientIps.get(client);
    }

    public int getVlcPort(int client) {
        return clientVlcPorts.get(client);
    }

    public int getSshPort(int client) {
        return clientSshPorts.get(client);
    }

    public List<Integer> getClientSshPorts() {
        return clientSshPorts;
    }

    public void setClientSshPorts(List<Integer> clientSshPorts) {
        this.clientSshPorts = clientSshPorts;
    }


    public List<String> getClientPasswords() {
        return clientPasswords;
    }

    public void setClientPasswords(List<String> clientPasswords) {
        this.clientPasswords = clientPasswords;
    }
    public String getClientPassword(int client){
        return clientPasswords.get(client);
    }

    public Boolean getVlcVersionCheck() {
        return vlcVersionCheck;
    }

    public void setVlcVersionCheck(Boolean vlcVersionCheck) {
        this.vlcVersionCheck = vlcVersionCheck;
    }

    public List<String> getClientUsername() {
        return clientUsername;
    }

    public String getClientUsername(int client){
        return clientUsername.get(client);
    }

    public void setClientUsername(List<String> clientUsername) {
        this.clientUsername = clientUsername;
    }

    public Boolean getQueryAll() {
        return queryAll;
    }

    public void setQueryAll(Boolean queryAll) {
        this.queryAll = queryAll;
    }
}
