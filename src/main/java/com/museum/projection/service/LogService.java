package com.museum.projection.service;

import com.museum.projection.dao.LogsDaoService;
import com.museum.projection.dto.*;
import com.museum.projection.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LogService {

    private final LogsDaoService logsDaoService;

    private static final Logger log = LoggerFactory.getLogger(LogService.class);

    public LogService(LogsDaoService logsDaoService) {
        this.logsDaoService = logsDaoService;
    }

    private List<LogDto> getLogs() {
        return logsDaoService.getLogs().stream().map(log -> {
            return new LogDto(log.getMessage(), StringUtils.toDateTime(log.getTimestamp()));
        }).collect(Collectors.toList());
    }

    public Page<LogDto> findPaginated(PageRequest pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<LogDto> list, logs = getLogs();

        if (logs.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, logs.size());
            list = logs.subList(startItem, toIndex);
        }
        return new PageImpl<LogDto>(list, PageRequest.of(currentPage, pageSize), logs.size());
    }

    public void createLog(String message) {
        log.debug(message);
        logsDaoService.insertLog(message);
    }
}
