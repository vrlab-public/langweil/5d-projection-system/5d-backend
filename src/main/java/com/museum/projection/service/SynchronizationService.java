package com.museum.projection.service;

import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.DisplaySetDto;
import com.museum.projection.dto.DisplayTrackDto;
import com.museum.projection.dto.VideoFileDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Synchronization service
 */
@Service
public class SynchronizationService {

    private final CustomConfig config;
    private static final Logger log = LoggerFactory.getLogger(SynchronizationService.class);

    final DisplaySetService displaySetService;
    final DisplayTrackService displayTrackService;
    final VideofileService videofileService;

    private DefaultSftpSessionFactory getFactory(int client) {
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();
        factory.setHost(config.getIp(client));
        factory.setPort(config.getSshPort(client));
        factory.setAllowUnknownKeys(true);
        factory.setUser(config.getClientUsername(client));
        factory.setPassword(config.getClientPassword(client));
        return factory;
    }

    /**
     *
     * @param config
     * @param displaySetService
     * @param displayTrackService
     * @param videofileService
     */
    public SynchronizationService(CustomConfig config, DisplaySetService displaySetService, DisplayTrackService displayTrackService, VideofileService videofileService) {
        this.config = config;
        this.displaySetService = displaySetService;
        this.displayTrackService = displayTrackService;
        this.videofileService = videofileService;
    }

    //    @Async

    /**
     * Synchronize all clients
     * @return
     */
    public ResponseData<Boolean> synchronizeAll() {
        System.out.println(config.getClientIps().get(0));
        System.out.println("Thread: " + Thread.currentThread());
        for (int i = 0; i < config.getClientCount(); i++) {
            ResponseData<Boolean> responseData = synchronizeClient(i);
            if (!responseData.getData()) return responseData;
        }
        return new ResponseData<>(true);
    }

    /**
     * Start synchronizaton on client with id clientId
     * @param clientId
     * @return
     */
    private ResponseData<Boolean> synchronizeClient(int clientId) {
        boolean errorFree = true;
        SftpSession session;
        try {
            session = getFactory(clientId).getSession();
        } catch (IllegalStateException ex) {
            log.error("failed to create SFTP Session", ex);
            return new ResponseData<Boolean>(false, "failed to create SFTP Session");
        }

        //COPY PLAYLIST
        File playlistFolder = new File(config.getPlaylistFolderPath() + (clientId + 1));
        if (!playlistFolder.exists() || !playlistFolder.isDirectory()) {
            String msg = "Folder " + config.getPlaylistFolderPath() + (clientId + 1) + " does not exist, or is not directory";
            log.error(msg);
            return new ResponseData<Boolean>(false, msg);
        }
        String dest = "";
        for (File file : Objects.requireNonNull(playlistFolder.listFiles())) {
            dest = config.getPlaylistFolderPath() + file.getName();
            errorFree &= uploadFile(clientId, session, dest, file);
        }


        File videoFolder = new File(config.getVideoFolderPath());
        if (!videoFolder.exists() || !videoFolder.isDirectory()) {
            String msg = "Folder " + config.getPlaylistFolderPath() + (clientId + 1) + " does not exist, or is not directory";
            log.error(msg);
            return new ResponseData<Boolean>(false, msg);
        }

        for (DisplaySetDto displaySetDto : displaySetService.getAllDisplaySets().stream().filter(DisplaySetDto::isPublished).collect(Collectors.toList())) {
            String pathToDisplaySetFolder = config.getVideoFolderPath().replace("otrojan", "user") + displaySetDto.getId();
            /* start */
            errorFree &= createFolder(clientId, session, pathToDisplaySetFolder); //TODO remove otrojan only for dev
            for (DisplayTrackDto trackDto : displayTrackService.getDisplayTracksByDisplaySetId(displaySetDto.getId())) {
                String pathToDisplayTrackFolder = pathToDisplaySetFolder + "/" + trackDto.getId();
                errorFree &= createFolder(clientId, session, pathToDisplayTrackFolder); //TODO remove otrojan only for dev
                for (VideoFileDto videoFileDto : videofileService.getAllVideoFilesByDisplayTrackId(trackDto.getId(), true)) {
                    //TODO here maybe copy only the files client needs
                    String serverFileName = displaySetDto.getId() + "-" + trackDto.getId() + "-" + videoFileDto.getFilename();
                    String destToVideoFile = pathToDisplayTrackFolder + "/" + serverFileName;
                    //test existing file
                    File localVideoFile = new File(videoFileDto.getFilepath());
                    if (!localVideoFile.exists() || !videoFolder.isDirectory()) {
                        log.error("Cannot upload non existing file " + localVideoFile.getAbsolutePath());
                        continue;
                    }
                    System.out.println("Uploading " + videoFileDto.getFilepath() + " to " + destToVideoFile);
                    errorFree &= uploadFile(clientId, session, destToVideoFile, localVideoFile);
                }
            }
        }
        session.close();
        if (!errorFree) return new ResponseData<>(false, "Some errors occurred, see log file for details");
        return new ResponseData<>(true);
    }


    private boolean uploadFile(int clientId, SftpSession session, String dest, File file) {
        if (session.exists(dest)) {
            log.info("File dest:" + dest + " to client " + (clientId + 1) + " already exists will overwrite");
        }
        InputStream resourceStream = null;
        try {
            resourceStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        try {
            session.write(resourceStream, dest);
            log.info("Copied file " + file.getName() + " to client " + (clientId + 1) + " dest:" + dest);
        } catch (IOException e) {
            log.error("Cannot create file " + dest, e);
            return false;
        }
        return true;
    }

    private boolean createFolder(int clientId, SftpSession session, String dest) {
        if (session.exists(dest)) {
            log.info("Folder dest:" + dest + " to client " + (clientId + 1) + " already exists");
            return true;
        }
        try {
            session.mkdir(dest);
            log.info("Folder dest:" + dest + " to client " + (clientId + 1) + " created");
        } catch (IOException e) {
            log.error("Cannot create directory " + dest, e);
            return false;
        }
        return true;
    }


}
