package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.dto.DisplaySetDto;
import com.museum.projection.model.TrackTypes;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.MultipartConfigElement;
import java.security.Principal;
import java.util.List;

/**
 * Service for model filling, called from Controllers
 */
@Service
public class ModelViewService {


    private final UserService userService;
    private final FolderService folderService;
    private final VideofileService videofileService;
    private final DisplayTrackService displayTrackService;
    private final DisplaySetService displaySetService;
    private final ClientService clientService;
    private final ControlService controlService;

    private final MultipartConfigElement multipartConfigElement;
    private final CustomConfig customConfig;

    /**
     *
     * @param userService
     * @param folderService
     * @param videofileService
     * @param displayTrackService
     * @param displaySetService
     * @param clientService
     * @param controlService
     * @param multipartConfigElement
     * @param customConfig
     */
    public ModelViewService(UserService userService, FolderService folderService, VideofileService videofileService, DisplayTrackService displayTrackService, DisplaySetService displaySetService, ClientService clientService, ControlService controlService, MultipartConfigElement multipartConfigElement, CustomConfig customConfig) {
        this.userService = userService;
        this.folderService = folderService;
        this.videofileService = videofileService;
        this.displayTrackService = displayTrackService;
        this.displaySetService = displaySetService;
        this.clientService = clientService;
        this.controlService = controlService;
        this.multipartConfigElement = multipartConfigElement;
        this.customConfig = customConfig;
    }


    /**
     *
     * @param principal
     * @param model
     * @return
     */
    //    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Model populateHeader(Principal principal, Model model) {
        model.addAttribute("name", principal.getName());
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    public Model populateUsersHeader(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "users");
        model.addAttribute("users", userService.getAllUsers());
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    public Model populateVideoFiles(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        model.addAttribute("videofiles", videofileService.getAllVideoFiles());
        model.addAttribute("maxFileSize", multipartConfigElement.getMaxFileSize());
        model.addAttribute("folders", folderService.getAllFolders());
        model.addAttribute("activeTabSecondary", "videofile");
        model.addAttribute("trackTypes", TrackTypes.getListOfTypes());
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    public Model populateVideoSet(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        model.addAttribute("activeTabSecondary", "videoset");
        model.addAttribute("videosets", displaySetService.getAllDisplaySets());
        model.addAttribute("clientConnected", controlService.areAllConnected());
        model.addAttribute("somethingIsPublished", displaySetService.getAllDisplaySets().stream().anyMatch(displaySetDto -> displaySetDto.isPublished() && !displaySetDto.isChanged()));
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    public Model populateVideoTrack(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        model.addAttribute("videotracks", displayTrackService.getAllDisplayTracks());
        model.addAttribute("clientCount", customConfig.getClientCount());
        model.addAttribute("videoFiles", videofileService.getAllVideoFiles());
        model.addAttribute("displaySets", displaySetService.getAllDisplaySets());
        model.addAttribute("activeTabSecondary", "videotrack");
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     * @return
     */
    public Model populateVideoFolder(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        model.addAttribute("folders", folderService.getAllFolders());
        model.addAttribute("activeTabSecondary", "videofolder");
        return model;
    }

    /**
     *
     * @param principal
     * @param model
     */
    public void populateMain(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "main");
        model.addAttribute("clientCount", customConfig.getClientCount());
        model.addAttribute("clients", clientService.getAllClients());
    }

    /**
     *
     * @param principal
     * @param model
     */
    public void populateAutonomous(Principal principal, Model model) {
        populateHeader(principal, model);
        model.addAttribute("activeTab", "control");
        List<DisplaySetDto> allDisplaySets = displaySetService.getAllDisplaySets();
        model.addAttribute("displaySets", allDisplaySets);
        model.addAttribute("clientCount", customConfig.getClientCount());
        model.addAttribute("autonomousList", displaySetService.getOrderedAutonomousDisplaySets());
    }
}
