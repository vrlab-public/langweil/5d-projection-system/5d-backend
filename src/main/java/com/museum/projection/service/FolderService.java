package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.FolderDaoService;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.FolderDto;
import com.museum.projection.model.Folder;
import com.museum.projection.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Folder service
 */
@Service
public class FolderService {

    private final FolderDaoService folderDaoService;
    private final CustomConfig config;

    private static final Logger log = LoggerFactory.getLogger(FolderService.class);

    @Autowired
    public FolderService(FolderDaoService folderDaoService, CustomConfig config) {
        this.folderDaoService = folderDaoService;
        this.config = config;
    }

    /**
     * Return all folders
     * @return
     */
    public List<FolderDto> getAllFolders() {
        List<FolderDto> folders = folderDaoService.getFolders().stream().map(folder -> new FolderDto(
                String.valueOf(folder.getId()),
                StringUtils.cutLastSlash(getFullRelativePath(folder.getPath(), folder.getParentFolderId())))).collect(Collectors.toList());
        return folders;
    }


    private ResponseData<Boolean> creatFolderAt(String path) {
        boolean flag = false;
        try {
            File file = new File(path + '/');
            flag = file.mkdir();
        } catch (Exception e) {
            log.error("Error creating directory " + path, e);
        }
        return new ResponseData<>(flag);
    }

    /**
     * Creates folder
     * @param parentFolderId
     * @param folderName
     * @return
     */
    public ResponseData<Boolean> createFolder(int parentFolderId, String folderName) {
        ResponseData<Boolean> ret;
        try {
            Optional<Folder> existing = folderDaoService.tryGetFolderByNameAndParentId(folderName + '/', parentFolderId);
            if (existing.isPresent()) {
                return new ResponseData(false, "folder " + StringUtils.cutLastSlash(getFullRelativePath(existing.get().getPath(), existing.get().getParentFolderId())) + " already exist", "folder");
            }
            //get parent
            Optional<Folder> chosenParent = folderDaoService.getFolderById(parentFolderId);
            String pathToParent = getFullRelativePath(chosenParent.orElseThrow().getPath(), chosenParent.orElseThrow().getParentFolderId());
            //try to create dir
            ResponseData<Boolean> booleanResponseData = creatFolderAt(StringUtils.cutLastSlash(config.getVideoFolderPath()) + pathToParent + folderName);
            if (!booleanResponseData.getData()) {
                log.error("Error creating directory " + StringUtils.cutLastSlash(config.getVideoFolderPath()) + pathToParent + folderName);
                return new ResponseData(false, "Error creating file at disk");
            }
            //if ok creat db
            ret = new ResponseData<>(folderDaoService.insertFolder(folderName + '/', parentFolderId).orElse(null));
        } catch (DuplicateKeyException e) {
            return new ResponseData(false);
        }
        return ret == null ? new ResponseData<>(false, "unkown error") : ret;
    }

    /**
     *
     * @param id
     * @return
     */
    public ResponseData<FolderDto> getFolderById(String id) {
        ResponseData<FolderDto> ret;

        try {
            ret = folderDaoService.getFolderById(Integer.parseInt(id)).map(folder ->
                    new ResponseData<>(new FolderDto(
                            String.valueOf(folder.getId()),
                            StringUtils.cutLastSlash(getFullRelativePath(folder.getPath(), folder.getParentFolderId()))))
            ).orElseThrow();
        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("Folder with id %s not found", id), "folderId");
        }
        return ret == null ? new ResponseData<>(null, "unkown error") : ret;
    }

    private String getFullRelativePath(String path, Integer parentFolderId) {
        String ret = path;
        Optional<Folder> parent = folderDaoService.tryGetFolderById(parentFolderId);
        if (parent.isPresent()) {
            ret = getFullRelativePath(parent.get().getPath(), parent.get().getParentFolderId()) + ret;
        }
        return ret;
    }




}
