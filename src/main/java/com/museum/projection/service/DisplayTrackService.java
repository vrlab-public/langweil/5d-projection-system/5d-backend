package com.museum.projection.service;


import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.DisplaySetDaoService;
import com.museum.projection.dao.DisplayTrackDaoService;
import com.museum.projection.dao.DisplayTrackToVideoFileDaoService;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.DisplayTrackDto;
import com.museum.projection.dto.VideoFileDto;
import com.museum.projection.model.DisplaySet;
import com.museum.projection.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
/**
 * DisplayTrack Business Service
 */
@Service
public class DisplayTrackService {

    private final DisplaySetDaoService displaySetDaoService;
    private final DisplayTrackDaoService displayTrackDaoService;
    private final DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService;
    private final VideofileService videofileService;
    private final UserService userService;

    private final CustomConfig config;
    private final Logger log = LoggerFactory.getLogger(DisplayTrackService.class);

    @Autowired
    public DisplayTrackService(DisplaySetDaoService displaySetDaoService, DisplayTrackDaoService displayTrackDaoService, DisplayTrackToVideoFileDaoService displayTrackToVideoFileDaoService, VideofileService videofileService, UserService userService, CustomConfig config) {
        this.displaySetDaoService = displaySetDaoService;
        this.displayTrackDaoService = displayTrackDaoService;
        this.displayTrackToVideoFileDaoService = displayTrackToVideoFileDaoService;
        this.videofileService = videofileService;
        this.userService = userService;
        this.config = config;
    }

    /**
     * returns all display tracks
     * @return
     */
    public List<DisplayTrackDto> getAllDisplayTracks() {
        List<DisplayTrackDto> displaySet = displayTrackDaoService.getDisplayTracks().stream().map(displaytrack -> new DisplayTrackDto(
                displaytrack.getId().toString(),
                displaySetDaoService.getDisplaySetById(displaytrack.getDisplaySetId()).get().getName(),
                displaytrack.getName(),
                userService.getUserById(String.valueOf(displaytrack.getCreatedBy())).getData().getName(),
                StringUtils.toDateTime(displaytrack.getCreatedCet()),
                displaytrack.getTrackType(),
                displaytrack.getDuration().toString(),
                displaytrack.getDescription())).collect(Collectors.toList());
        return displaySet;
    }

    /**
     * Creates display track for displaySet with id, changes display set as changed
     * @param displaySetId
     * @param currentUserId
     * @param name
     * @param description
     * @param videoFileIds
     * @return
     */
    public ResponseData<Boolean> createDisplayTrack(String displaySetId, String currentUserId, String name, String description, List<String> videoFileIds) {
        Optional<DisplaySet> displaySet = displaySetDaoService.getDisplaySetById(Integer.parseInt(displaySetId));
        if (displaySet.isEmpty()) return new ResponseData<>(false, "cannot find displaySet with id " + displaySetId);

        //test if all videosets
        HashSet<String> types = new HashSet<>();
        float shortest = Float.MAX_VALUE;
        for (String videoFileId : videoFileIds) {
            ResponseData<VideoFileDto> videoFile = videofileService.getVideoFileById(videoFileId, false);
            if (videoFile.getData() == null) {
                log.error("cannot find videoFile with id " + videoFileId);
                return new ResponseData<>(false, "cannot find videoFile with id " + videoFileId);
            }
            types.add(videoFile.getData().getVideoFileType().toUpperCase());
            float durationOfCurrent = Float.parseFloat(videoFile.getData().getDuration());
            if (durationOfCurrent < shortest) shortest = durationOfCurrent;
        }

        //create videoTrack
        Optional<Long> createdDisplayTrackId = displayTrackDaoService.insertDisplayTrack(name, displaySetId, Integer.parseInt(currentUserId), shortest, description, String.join(",", types));
        if (createdDisplayTrackId.isEmpty()) {
            return new ResponseData<>(false, "display track could not be created");
        }
        String videoFileId;
        for (int client = 0; client < config.getClientCount(); client++) {
            videoFileId = videoFileIds.get(client);
            Optional<Long> createdDisplayTrackToVideoFileId = displayTrackToVideoFileDaoService.insertDisplayTrackToVideoFile(createdDisplayTrackId.get().toString(), videoFileId, String.valueOf(client + 1), currentUserId);
            if (createdDisplayTrackToVideoFileId.isEmpty())
                return new ResponseData<>(false, "display track to video file could not be created");
        }
        //set displayset as changed
        displaySetDaoService.updateDisplaySet(displaySetId, displaySet.get().getName(), displaySet.get().getPublished(), displaySet.get().getAutonomous(), true, displaySet.get().getDuration() + shortest);
        return new ResponseData<>(true);
    }

    /**
     *
     * @param displayTrackId
     * @return
     */
    public ResponseData<DisplayTrackDto> getDisplayTrackById(String displayTrackId) {
        ResponseData<DisplayTrackDto> ret;

        try {
            ret = displayTrackDaoService.getDisplayTrackById(Integer.parseInt(displayTrackId)).map(displaytrack ->
                    new ResponseData<>(new DisplayTrackDto(
                            displaytrack.getId().toString(),
                            displaySetDaoService.getDisplaySetById(displaytrack.getDisplaySetId()).get().getName(),
                            displaytrack.getName(),
                            userService.getUserById(String.valueOf(displaytrack.getCreatedBy())).getData().getName(),
                            StringUtils.toDateTime(displaytrack.getCreatedCet()),
                            displaytrack.getTrackType(),
                            displaytrack.getDuration().toString(),
                            displaytrack.getDescription()))).orElseThrow();

        } catch (EmptyResultDataAccessException e) {
            return new ResponseData(null, String.format("Display track with id %s not found", displayTrackId), "displayTrack");
        }
        return ret == null ? new ResponseData<>(null, "unknown error") : ret;
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    public List<DisplayTrackDto> getDisplayTracksByDisplaySetId(String displaySetId) {

        List<DisplayTrackDto> displaySet = displayTrackDaoService.getDisplayTrackByDisplaysetId(displaySetId).stream().map(displaytrack -> new DisplayTrackDto(
                displaytrack.getId().toString(),
                displaySetDaoService.getDisplaySetById(displaytrack.getDisplaySetId()).get().getName(),
                displaytrack.getName(),
                userService.getUserById(String.valueOf(displaytrack.getCreatedBy())).getData().getName(),
                StringUtils.toDateTime(displaytrack.getCreatedCet()),
                displaytrack.getTrackType(),
                displaytrack.getDuration().toString(),
                displaytrack.getDescription())).collect(Collectors.toList());
        return displaySet;
    }


}
