package com.museum.projection.service;

import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.other.ParsedPlaylistTrack;
import com.museum.projection.dto.other.PlaylistTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Playlist service
 */
@Service
public class PlaylistService {

    private static final Logger log = LoggerFactory.getLogger(PlaylistService.class);
    private final CustomConfig config;

    private final String HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<playlist xmlns=\"http://xspf.org/ns/0/\" xmlns:vlc=\"http://www.videolan.org/vlc/playlist/ns/0/\" version=\"1\">";

    private final String PLAYLIST_START = "+----[ Playlist - playlist ]";
    private final String PLAYLIST_END = "+----[ End of playlist ]";
    private final String PLAYLIST_1 = "| 1 - Playlist";
    private final String PLAYLIST_PARSE_REG_NAME_ = "[|]\\s{2,3}[*]*\\d+\\s*[-]\\s{1}(?<name>.+?)\\s";
    private final String PLAYLIST_PARSE_REG_ID_NAME = "[|]\\s{2,3}[*]*(?<id>\\d+?)\\s*[-]\\s{1}(?<name>.+?)\\s";
    private final String PLAYLIST_PARSE_REG_RUN_ID_NAME = "[|]\\s{2,3}(?<run>[*]*?)(?<id>\\d+?)\\s*[-]\\s{1}(?<name>.+?)\\s";
    private final String PLAYLIST_PARSE_REG_ALL = "[|]\\s{2,3}[*]*\\d+\\s*[-]\\s{1}(?<name>.+?)\\s([(](?<time>.+?)[)]\\s)*\\[\\w*\\s(?<played>\\d*)\\s[\\w]*]";
    private final String PLAYLIST_PARSE_REG_NAME_TIME = "[|]\\s{2,3}[*]*\\d+\\s*[-]\\s{1}(?<name>.+?)\\s([(](?<time>.+?)[)]\\s)";


    public PlaylistService(CustomConfig config) {
        this.config = config;
    }

    /**
     * Generates .xspf style xml playlist from valid playlistData
     * @param displaySetName
     * @param displaySetId
     * @param playlistsData
     * @return
     */
    public ResponseData< String[]> generatePlaylists(String displaySetName, String displaySetId, ArrayList<PlaylistTrack>[] playlistsData) {
        String[] ret = new String[playlistsData.length];
        StringBuilder sb;
        for (int client = 0; client < playlistsData.length; client++) {
            sb = new StringBuilder();
            sb.append(HEADER);
            addTitle(sb, displaySetName);
            sb.append("<trackList>");
            PlaylistTrack tr = null;
            for (int j = 0; j < playlistsData[client].size(); j++) {
                tr = playlistsData[client].get(j);
                addTrack(sb, tr.getPath(), displaySetName, displaySetId, tr.getDisplayTrackName(), tr.getDisplayTrackId(), tr.getFileName(), tr.getDuration(), j);
            }
            sb.append("</trackList>");
            sb.append("<extension application=\"http://www.videolan.org/vlc/playlist/0\">");
            for (int j = 0; j < playlistsData[client].size(); j++) {
                sb.append("<vlc:item tid=\"");
                sb.append(j);
                sb.append("\"/>");
            }
            sb.append("</extension>");
            sb.append("</playlist>");
            ret[client] = sb.toString();
        }
        return new ResponseData<>(ret);
    }

    /**
     * Returns converted playlist name
     * @param displaySetName
     * @return
     */
    public static String getPlaylistName(String displaySetName) {
        return displaySetName.replaceAll("\\s+", "_") + ".xspf";
    }

    private void addTitle(StringBuilder sb, String title) {
        sb.append("<title>");
        sb.append(title);
        sb.append("</title>");
    }

    private void addTrack(StringBuilder sb, String path, String displaySetName, String displaySetId, String displayTrackName, String displayTrackId, String filename, Float duration, int item) {
        sb.append("<track>");
        sb.append("<location>file://");
        String newPath = toServerFileName(config.getVideoFolderPath(), displaySetId, displayTrackId,filename);
        sb.append(newPath);
        sb.append("</location>");

        sb.append("<duration>");
        sb.append((long) (duration * 1000));
        sb.append("</duration>");

        sb.append("<extension application=\"http://www.videolan.org/vlc/playlist/0\">");
        sb.append("<vlc:id>");
        sb.append(item);
        sb.append("</vlc:id>");
        sb.append("<vlc:option>recursive=collapse</vlc:option>");
        sb.append("</extension>");

        sb.append("</track>");
    }

    /**
     * Saves playlists into playlits folder from config
     * @param name
     * @param playlists
     */
    public void savePlaylist(String name, String[] playlists) {
        String playlistFolder = config.getPlaylistFolderPath();
        String playlistName = name.replaceAll("\\s+", "_");
        for (int i = 0; i < config.getClientCount(); i++) {
            File folder = new File(playlistFolder + (i + 1));
            if (!folder.exists()) {
                boolean created = false;
                try {
                    created = folder.mkdir();
                } catch (Exception e) {
                    log.error("Error creating directory " + (i + 1), e);
                }
                if (!created) {
                    log.error("Folder " + (i + 1) + " not created");
                    return;
                }
            }
            File file = new File(playlistFolder + (i + 1) + "/" + playlistName + ".xspf");
            FileWriter fr = null;
            try {
                fr = new FileWriter(file);
                fr.write(playlists[i]);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * Parse received VLC playlist from Remote clients
     * @param playlist
     * @return
     */
    public List<ParsedPlaylistTrack> parseTitlesPlaylist(List<String> playlist) {
        ArrayList<ParsedPlaylistTrack> ret = new ArrayList<>();
        if (playlist.isEmpty() || playlist.get(0) == null || !playlist.get(0).equals(PLAYLIST_START) || playlist.get(1) == null || !playlist.get(1).equals(PLAYLIST_1)) {
            log.error("invalid playlist input of " + playlist.toString());
            return List.of();
        }

        final Pattern PATTERN = Pattern.compile(PLAYLIST_PARSE_REG_RUN_ID_NAME);

        Matcher normal, chosen;
        for (int i = 2; i < playlist.size() - 2; i++) {
            normal = PATTERN.matcher(playlist.get(i));

            if (!normal.find()) {
                log.error("Cannot match incoming playlist " + playlist.toString() + " item from client with reg");
                return List.of();
            }
            ret.add(new ParsedPlaylistTrack(normal.group("run").equals("*"), Integer.parseInt(normal.group("id")),normal.group("name")));
        }
        return ret;
    }

    public static String toServerFileName(String videoFolderPath, String displaySetId, String displayTrackId, String fileName){
        String serverFileName = displaySetId + "-" + displayTrackId + "-" + fileName;
        return  videoFolderPath.replace("otrojan", "user") + displaySetId.toLowerCase().replaceAll("\\s+", "_") + "/" + displayTrackId.toLowerCase().replaceAll("\\s+", "_") + "/" + serverFileName;
    }

}
