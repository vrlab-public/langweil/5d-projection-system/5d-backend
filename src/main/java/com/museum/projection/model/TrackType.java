package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class TrackType {


    @NotBlank
    private final Integer id;

    @NotBlank
    private final String name;

    private final TrackTypes type;


    public TrackType(@JsonProperty("id") Integer id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
        this.type = TrackTypes.picture;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public TrackTypes getType() {
        return type;
    }
}
