package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * Database model
 */
public class Account {

    private final Integer id;

    @NotBlank
    private final String username;

    @NotBlank
    private final String password;

    private final String roles;

    @NotBlank
    private final Timestamp createdCet;

    @NotBlank
    private final boolean isEnabled;

    public Account(@JsonProperty("id") Integer id, @JsonProperty("username") String username, @JsonProperty("password") String password, @JsonProperty("roles") String roles, @JsonProperty("createdCet") Timestamp createdCet, @JsonProperty("active") boolean enabled) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.createdCet = createdCet;
        this.isEnabled = enabled;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRoles() {
        return roles;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public Timestamp getCreatedCet() {
        return createdCet;
    }
}
