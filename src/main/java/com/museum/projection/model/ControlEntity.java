package com.museum.projection.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.museum.projection.util.validation.OnlyFilename;
import com.museum.projection.util.validation.OnlyNumbers;

import javax.validation.constraints.NotNull;

/**
 * Database model
 */
public class ControlEntity {

    @OnlyNumbers
    @JsonProperty("displaySetId")
    @NotNull
    public String displaySetId;

    @OnlyNumbers
    @JsonProperty("displayTrackId")
    @NotNull
    public String displayTrackId;

    @OnlyFilename
    @JsonProperty("video")
    @NotNull
    public String videoName;
}
