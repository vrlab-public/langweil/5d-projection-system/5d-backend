package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class Log {


    @NotBlank
    @NotNull
    private final String message;

    @NotBlank
    @NotNull
    private final Timestamp timestamp;


    public Log(@JsonProperty("message") String message, @JsonProperty("timestamp") Timestamp timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }
}
