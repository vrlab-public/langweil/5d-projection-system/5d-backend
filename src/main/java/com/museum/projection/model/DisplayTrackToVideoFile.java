package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

/**
 * Database model
 */
public class DisplayTrackToVideoFile {

    @NotBlank
    private final Integer id;

    @NotBlank
    private final Integer displaySetId;

    @NotBlank
    private final Integer displayTrackId;

    @NotBlank
    private final Integer clientId;

    @NotBlank
    private final Integer createdBy;

    @NotBlank
    private final Timestamp createdCet;

    public DisplayTrackToVideoFile(@JsonProperty("displayTrackToVideoFileId") Integer displayTrackToVideoFileId,
                                   @JsonProperty("displayTrackId") Integer displayTrackId,
                                   @JsonProperty("displaySetId") Integer displaySetId,
                                   @JsonProperty("client")Integer clientId,
                                   @JsonProperty("createdBy") Integer createdBy,
                                   @JsonProperty("createdCet") Timestamp createdCet) {
        this.id = displayTrackToVideoFileId;
        this.displaySetId = displaySetId;
        this.displayTrackId = displayTrackId;
        this.clientId = clientId;
        this.createdCet = createdCet;
        this.createdBy = createdBy;
    }


    public Integer getId() {
        return id;
    }


    public Integer getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedCet() {
        return createdCet;
    }

    public Integer getDisplaySetId() {
        return displaySetId;
    }

    public Integer getDisplayTrackId() {
        return displayTrackId;
    }
}
