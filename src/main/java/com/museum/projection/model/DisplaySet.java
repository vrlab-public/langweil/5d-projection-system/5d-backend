package com.museum.projection.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * Database model
 */
public class DisplaySet {

    @NotBlank
    private final Integer id;

    @NotBlank
    private final String name;

    @NotBlank
    private final Integer createdBy;

    @NotBlank
    private final Timestamp createdCet;

    @NotNull
    private final Float duration;

    @NotNull
    private final Boolean published;

    @NotNull
    private final Boolean changed;

    @NotNull
    private final Integer autonomous;

    private final String description;

    public DisplaySet(@JsonProperty("id") Integer id,
                      @JsonProperty("name") String name,
                      @JsonProperty("createdBy") Integer createdBy,
                      @JsonProperty("createdCet") Timestamp createdCet,
                      @JsonProperty("duration") Float duration,
                      @JsonProperty("published") Boolean published,
                      @JsonProperty("changed") Boolean changed,
                      @JsonProperty("autonomous") Integer autonomous,
                      @JsonProperty("description") String description) {
        this.id = id;
        this.name = name;
        this.createdCet = createdCet;
        this.createdBy = createdBy;
        this.duration = duration;
        this.published = published;
        this.changed = changed;
        this.autonomous = autonomous;
        this.description = description;
    }


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public Timestamp getCreatedCet() {
        return createdCet;
    }

    public Boolean getPublished() {
        return published;
    }

    public Integer getAutonomous() {
        return autonomous;
    }

    public Float getDuration() {
        return duration;
    }

    public Boolean getChanged() {
        return changed;
    }

    public String getDescription() {
        return description;
    }
}
