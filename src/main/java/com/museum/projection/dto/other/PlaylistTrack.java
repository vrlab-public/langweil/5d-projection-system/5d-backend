package com.museum.projection.dto.other;

public class PlaylistTrack {
    private String path;
    private String fileName;
    private Float duration;
    private String displayTrackId;
    private String displayTrackName;

    public PlaylistTrack(String path, String fileName, Float duration, String displayTrackName, String displayTrackId) {
        this.path = path;
        this.fileName = fileName;
        this.duration = duration;
        this.displayTrackId = displayTrackId;
        this.displayTrackName = displayTrackName;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDisplayTrackId() {
        return displayTrackId;
    }

    public void setDisplayTrackId(String displayTrackId) {
        this.displayTrackId = displayTrackId;
    }

    public String getDisplayTrackName() {
        return displayTrackName;
    }

    public void setDisplayTrackName(String displayTrackName) {
        this.displayTrackName = displayTrackName;
    }
}
