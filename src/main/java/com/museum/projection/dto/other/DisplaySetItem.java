package com.museum.projection.dto.other;

public class DisplaySetItem {


    private int order;

    public DisplaySetItem() {

    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
