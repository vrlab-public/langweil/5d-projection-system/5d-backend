package com.museum.projection.dto.other;

public class VideoFileItem {

    private String id;

    public VideoFileItem() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
