package com.museum.projection.dto.forms;

import com.museum.projection.dto.other.DisplaySetItem;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * Object for Form validation
 */
public class AutonomousForm {


    @NotNull
    @NotEmpty(message = "List of display sets must not be empty")
    private ArrayList<DisplaySetItem> autonomousSets;


    public AutonomousForm(
            @NotNull @NotEmpty(message = "List of display sets must not be empty") ArrayList<DisplaySetItem> autonomousSets) {

        this.autonomousSets = autonomousSets;
    }


    public ArrayList<DisplaySetItem> getAutonomousSets() {
        return autonomousSets;
    }

    public void setAutonomousSets(ArrayList<DisplaySetItem> autonomousSets) {
        this.autonomousSets = autonomousSets;
    }


}