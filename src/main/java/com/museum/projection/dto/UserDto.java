
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class UserDto {
    private String id;
    private String name;
    private String isEnabled;
    private String roles;
    private String createdCet;

    public UserDto(String id, String name, String isEnabled, String roles, String createdCet){
        this.id=id;
        this.name=name;
        this.isEnabled=isEnabled;
        this.roles=roles;
        this.createdCet = createdCet;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(String isEnabled) {
        this.isEnabled = isEnabled;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getCreatedCet() {
        return createdCet;
    }
}
