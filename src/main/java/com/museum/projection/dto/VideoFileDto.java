
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class VideoFileDto {
    private String id;
    private String filename;
    private String filepath;
    private String fileSize;
    private String duration;
    private String description;
    private String createdCet;
    private String createdByUsername;
    private String clientSynchronized;
    private String videoFileType;


    public VideoFileDto(String id, String filename, String filepath, String fileSize, String duration, String description, String createdCet, String createdByUsername, String clientSynchronized, String videoFileType) {
        this.id = id;
        this.filename = filename;
        this.filepath = filepath;
        this.fileSize = fileSize;
        this.duration = duration;
        this.description = description;
        this.createdCet = createdCet;
        this.createdByUsername = createdByUsername;
        this.clientSynchronized = clientSynchronized;
        this.videoFileType = videoFileType;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedCet() {
        return createdCet;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public void setCreatedByUsername(String createdByUsername) {
        this.createdByUsername = createdByUsername;
    }

    public String getClientSynchronized() {
        return clientSynchronized;
    }

    public void setClientSynchronized(String clientSynchronized) {
        this.clientSynchronized = clientSynchronized;
    }

    public String getVideoFileType() {
        return videoFileType;
    }

    public void setVideoFileType(String videoFileType) {
        this.videoFileType = videoFileType;
    }
}
