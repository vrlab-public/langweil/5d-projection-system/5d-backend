
package com.museum.projection.dto;

/**
 * Data Transfer Object
 */
public class FolderDto {
    private String id;
    private String folder;

    public FolderDto(String id, String folder) {
        this.id = id;
        this.folder = folder;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }
}
