package com.museum.projection.clients;

import com.museum.projection.service.ControlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for VLC remote client
 */
public class VlcClient implements RemoteClient {

    private static final int SOCKET_TIMEOUT = 900;
    private static final int SOCKET_SINGLE_TIMEOUT = 200;
    private static final int SOCKET_TIMEOUT_LIMIT = 1;
    private static String[] STARTUP_RESPONSE = {"VLC media player 3.0.10 Vetinari", "Command Line Interface initialized. Type `help' for help."};
    private static final String PLAYLIST_END = "+----[ End of playlist ]";
    private static final String WAITING_COMMAND = "> ";
    private static final String IS_PLAYING_TRUE = "> 1";
    private static final String IS_PLAYING_FALSE = "> 0";

    private static final Logger log = LoggerFactory.getLogger(VlcClient.class);

    private volatile Socket socket;
    private volatile PrintWriter out;
    private volatile BufferedReader in;


    private AtomicInteger timeoutsCounter = new AtomicInteger(0);
    private AtomicInteger lostConnectionTimeoutCounter = new AtomicInteger(0);
    private AtomicBoolean connectionReady = new AtomicBoolean(false);
    private AtomicBoolean isPlaying = new AtomicBoolean(false);

    private final String ip;
    private final int port;
    private final int id;
    private final boolean vlcVersionCheck;


    @Override
    public void initCheck() throws InterruptedException {
        if (!isConnectionReady()) {
            startConnection();
        }
    }


    @Override
    public void maintainCheck() throws InterruptedException {
        if (isConnectionReady()) {
            log.info("Maintaining connection to " + ip + ":" + port);
            verifyConnection();
        }
    }

    /**
     * Constructor
     * @param address
     * @param port
     * @param id
     * @param vlcVersionCheck
     */
    public VlcClient(String address, int port, int id, boolean vlcVersionCheck) {
        this.ip = address;
        this.port = port;
        this.id = id;
        this.vlcVersionCheck = vlcVersionCheck;
    }

    /**
     * Initializes connection, opens sockets, validates received headers
     * @throws InterruptedException
     */
    private void startConnection() throws InterruptedException {
        log.info("Starting connection to " + ip + ":" + port);
        lostConnectionTimeoutCounter.set(0);
        timeoutsCounter.set(0);
        try {
            socket = new Socket(ip, port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socket.setSoTimeout(SOCKET_TIMEOUT);
        } catch (ConnectException | NoRouteToHostException ce) {
            log.warn("Connect to " + ip + ":" + port + " has not yet been established");
            if (socket != null) {
                log.info("Closing socket " + ip + ":" + port);
                closeCurrentConnectAttempt();
            }
            setConnectionReady(false);
            return;

        } catch (IOException e) {
            log.error("Error during connection to" + ip + ":" + port, e);
            closeCurrentConnectAttempt();
            return;
        }

        List<String> received = new ArrayList<>();
        String line;
        boolean responseOk = true;
        try {
            for (int i = 0; i < 2; i++) {
                line = in.readLine();
                log.info("start_" + line);
                received.add(line);
                if(vlcVersionCheck || i != 0)  responseOk &= received.get(i).equals(STARTUP_RESPONSE[i]);
            }
            String next = Character.toString((char) in.read()) + Character.toString((char) in.read());
            received.add(next);
            responseOk &= received.get(2).equals(WAITING_COMMAND);
        } catch (SocketTimeoutException | SocketException e) {
            String msg = "";
            if (e instanceof SocketTimeoutException) {
                msg = "Socket " + ip + ":" + port + " buffer read exited on timeout of " + SOCKET_TIMEOUT;
            } else {
                msg = "Socket " + ip + ":" + port + " exception";
            }
//            log.error(msg, e.getCause()); TODO UNCOMMENT
            closeCurrentConnectAttempt();
            return;
        } catch (IOException e) {
            log.error("Error during connection to" + ip + ":" + port, e);
            closeCurrentConnectAttempt();
            return;
        }
        setConnectionReady(responseOk);
        log.info("Connection to " + ip + ":" + port + " has been established " + isConnectionReady());

    }

    /**
     * verifies connection by sending is_playing command and waiting for response
     */
    private void verifyConnection() {
        List<String> received = null;
        try {
            received = sendMessage("is_playing", 1, 1);
        } catch (SocketTimeoutException | SocketException e) {
//            log.error("Socket " + ip + ":" + port + " buffer read exited on timeout of " + SOCKET_TIMEOUT, e.getCause());TODO UNCOMMENT
            timeoutsCounter.incrementAndGet();
            //TODO out of sync
            if (timeoutsCounter.get() > SOCKET_TIMEOUT_LIMIT) {
                log.error("Socket " + ip + ":" + port + " timeout limit of " + SOCKET_TIMEOUT_LIMIT + " exceeded, shutting the connection");
                timeoutsCounter.set(0);
                closeCurrentConnectAttempt();
            }
            return;
        } catch (IOException e) {
            log.error("Connection " + ip + ":" + port + " verified as not active, shutting down", e.getCause());
            closeCurrentConnectAttempt();
            return;
        }
        if (received.size() != 1 || received.get(0) == null) {
            log.error("Connection " + ip + ":" + port + " unexpected response. Expected " + IS_PLAYING_FALSE + " or " + IS_PLAYING_TRUE + ", received " + Arrays.toString(received.toArray()));
            lostConnectionTimeoutCounter.incrementAndGet();
            //TODO out of sync
            if (lostConnectionTimeoutCounter.get() > SOCKET_TIMEOUT_LIMIT) {
                log.error("Connection " + ip + ":" + port + " timeout limit of " + SOCKET_TIMEOUT_LIMIT + " exceeded, shutting the connection");
                closeCurrentConnectAttempt();
            }
            return;
        }
        isPlaying.set(received.get(0).equals("1"));
        boolean status = received.get(0).equals("0") || received.get(0).equals("1");
        setConnectionReady(status);
        if (status) {
            lostConnectionTimeoutCounter.set(0);
            timeoutsCounter.set(0);
        }
    }


    private void closeCurrentConnectAttempt() {
        setConnectionReady(false);
        close();

    }

    /**
     * Returns true if connection is ready
     * @return
     */
    public synchronized boolean isConnectionReady() {
        return connectionReady.get();
    }

    private synchronized void setConnectionReady(boolean connectionReady) {
        this.connectionReady.set(connectionReady);
    }

    /**
     * Sends command for playlist
     * @return
     */
    public List<String> sendForPlaylist() {
        long start = System.currentTimeMillis();
        if (!isConnectionReady()) {
            log.warn("Attempt to send to non established connection");
            return List.of();
        }
        try {
            out.println("playlist");
            List<String> resp = new ArrayList<>();
            String line = in.readLine();
            while (line != null) {
                resp.add(line);
                if (line.equals(PLAYLIST_END)) break;
                line = in.readLine();
            }
            String next = (Character.toString((char) in.read()) + Character.toString((char) in.read()));
            if (!next.equals(WAITING_COMMAND)) {
                log.error("Not received next command character");
            }
            System.out.println("Send for playlist" + (System.currentTimeMillis() - start));
            return resp;
        } catch (SocketTimeoutException | SocketException e) {
            log.error("Socket " + ip + ":" + port + " buffer read exited on timeout of " + SOCKET_TIMEOUT, e.getCause());
            timeoutsCounter.incrementAndGet();
            if (timeoutsCounter.get() > SOCKET_TIMEOUT_LIMIT) {
                log.error("Socket " + ip + ":" + port + " timeout limit of " + SOCKET_TIMEOUT_LIMIT + " exceeded, shutting the connection");
                closeCurrentConnectAttempt();
            }

        } catch (IOException e) {
            e.printStackTrace();
            closeCurrentConnectAttempt();
        }
        return List.of();
    }


    @Override
    public int getId() {
        return id;
    }

    /**
     * Forward command if connection ready
     * @param msg
     * @param expectedLines
     * @return
     */
    public List<String> sendCmd(String msg, int expectedLines) {
        if (!isConnectionReady()) {
            log.warn("Attempt to send to non established connection");
            return List.of();
        }
        try {
            return sendMessage(msg, expectedLines, 1);
        } catch (SocketTimeoutException | SocketException e) {
            log.error("Socket " + ip + ":" + port + " buffer read exited on timeout of " + SOCKET_TIMEOUT, e.getCause());
            timeoutsCounter.incrementAndGet();
            if (timeoutsCounter.get() > SOCKET_TIMEOUT_LIMIT) {
                log.error("Socket " + ip + ":" + port + " timeout limit of " + SOCKET_TIMEOUT_LIMIT + " exceeded, shutting the connection");
                closeCurrentConnectAttempt();
            }

        } catch (IOException e) {
            e.printStackTrace();
            closeCurrentConnectAttempt();
        }
        return null;
    }

    @Override
    public boolean isConnected() {
        return connectionReady.get();
    }

    @Override
    public boolean isPlaying() {
        return isPlaying.get();
    }

    private synchronized List<String> sendMessage(String msg, int expectedLines, int wait) throws IOException {
        long start = System.currentTimeMillis();
        out.println(msg);
        ArrayList<String> resp = new ArrayList<>();
        String line = null;

        for (int i = 0; i < expectedLines; i++) {
            line = in.readLine();
            resp.add(line);
        }
        String next = (Character.toString((char) in.read()) + Character.toString((char) in.read()));
        if (!next.equals(WAITING_COMMAND)) {
            log.error("Not received next command character");
        }
        log.debug("Send " + msg + " received in " + (System.currentTimeMillis() - start) + "ms " + Arrays.toString(resp.toArray()));

        return resp;
    }

    /**
     *  Closes the socket to the client
     */
    public void close() {
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            log.error("Socket " + ip + ":" + port + "cannot be closed", e);
        }

    }


}
