package com.museum.projection.clients;

import java.util.List;

/**
 * Interface for remote clients
 */
public interface RemoteClient {
    /**
     *
     * @return
     */
    public int getId();

    /**
     *
     * @param msg
     * @param expectedLines
     * @return
     */
    public List<String> sendCmd(String msg, int expectedLines);

    /**
     *
     * @return
     */
    public List<String> sendForPlaylist();

    /**
     *
     * @return
     */
    public boolean isConnected();

    /**
     *
     * @return
     */
    public boolean isPlaying();

    /**
     *
     * @throws InterruptedException
     */
    public void maintainCheck() throws InterruptedException;

    /**
     *
     * @throws InterruptedException
     */
    public void initCheck() throws InterruptedException;
}

