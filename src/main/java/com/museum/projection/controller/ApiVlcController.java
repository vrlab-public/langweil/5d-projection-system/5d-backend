package com.museum.projection.controller;


import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.DisplaySetDto;
import com.museum.projection.dto.DisplayTrackDto;
import com.museum.projection.model.ControlEntity;
import com.museum.projection.service.ControlService;
import com.museum.projection.service.DisplaySetService;
import com.museum.projection.service.DisplayTrackService;
import com.museum.projection.service.VideofileService;
import com.museum.projection.util.validation.OnlyNumbers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * Controller for remote client control (VLC)
 */
@RestController
@RequestMapping("/api/vlc")
public class ApiVlcController {


    @Resource
    public DisplayTrackService displayTrackService;
    @Resource
    public DisplaySetService displaySetService;
    @Resource
    public ControlService controlService;
    @Resource
    public VideofileService videofileService;

    /**
     *
     * @return
     */
    @PostMapping(value = "/play", produces = "application/json")
    public ResponseEntity play() {
        controlService.play();
        return new ResponseEntity<>(
                "Video set to play", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/pause", produces = "application/json")
    public ResponseEntity pause() {
        controlService.pause();
        return new ResponseEntity<>(
                "Video set to pause", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/stop", produces = "application/json")
    public ResponseEntity stop() {
        controlService.stop();
        return new ResponseEntity<>(
                "Video set to stop", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/next", produces = "application/json")
    public ResponseEntity next() {
        controlService.next();
        return new ResponseEntity<>(
                "Video set to next", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/prev", produces = "application/json")
    public ResponseEntity prev() {
        controlService.prev();
        return new ResponseEntity<>(
                "Video set to prev", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/repeat", produces = "application/json")
    public ResponseEntity repeat() {
        controlService.repeat();
        return new ResponseEntity<>(
                "Video set to repeat", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/shuffle", produces = "application/json")
    public ResponseEntity shuffle() {
        controlService.shuffle();
        return new ResponseEntity<>(
                "Video set to shuffle", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @PostMapping(value = "/clear", produces = "application/json")
    public ResponseEntity clear() {
        controlService.clear();
        return new ResponseEntity<>(
                "Video set to clear", HttpStatus.OK);
    }

    /**
     *
     * @param item
     * @return
     */
    @PostMapping(value = "/goto/{item}", produces = "application/json")
    public ResponseEntity gotoitem(@PathVariable("item") @OnlyNumbers String item) {
        controlService.gotoitem(Integer.parseInt(item));
        return new ResponseEntity<>(
                "Video set to goto", HttpStatus.OK);
    }

    /**
     *
     * @param from
     * @param to
     * @return
     */
    @PostMapping(value = "/move/{from}/{to}", produces = "application/json")
    public ResponseEntity move(@PathVariable("from") @OnlyNumbers String from, @PathVariable("to") @OnlyNumbers String to) {
        controlService.move(Integer.parseInt(from), Integer.parseInt(to));
        return new ResponseEntity<>(
                "Video set to moved from " + from + " to " + to, HttpStatus.OK);
    }

    /**
     *
     * @param second
     * @return
     */
    @PostMapping(value = "/seek/{second}", produces = "application/json")
    public ResponseEntity seek(@PathVariable("second") @OnlyNumbers String second) {
        controlService.seek(Integer.parseInt(second));
        return new ResponseEntity<>(
                "Video set to goto", HttpStatus.OK);
    }

    /**
     *
     * @param displaySetId
     * @param displayTrackId
     * @return
     */
    @PostMapping(value = "/add/{displaySetId}/{displayTrackId}", produces = "application/json")
    public ResponseEntity add(@PathVariable("displaySetId") @OnlyNumbers String displaySetId, @PathVariable("displayTrackId") @OnlyNumbers String displayTrackId) {
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(displaySetId);
        if (displaySet.getData() == null) return new ResponseEntity<>(
                "DisplaySet " + displaySetId + " not found", HttpStatus.NOT_FOUND);

        ResponseData<DisplayTrackDto> displayTrack = displayTrackService.getDisplayTrackById(displayTrackId);
        if (displayTrack.getData() == null) return new ResponseEntity<>(
                "DisplayTrack " + displayTrackId + " not found", HttpStatus.NOT_FOUND);

        controlService.addVideo(displaySet.getData().getId(), displayTrack.getData().getId(), videofileService.getAllVideoFilesByDisplayTrackId(displayTrackId, true));
        return new ResponseEntity<>(
                "Track " + displaySet.getData().getName() + " set to play", HttpStatus.OK);
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    @PostMapping(value = "/addplaylist/{displaySetId}", produces = "application/json")
    public ResponseEntity addPlaylist(@PathVariable("displaySetId") @OnlyNumbers String displaySetId) {
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(displaySetId);
        if (displaySet.getData() == null) return new ResponseEntity<>("Playlist not found", HttpStatus.NOT_FOUND);
        controlService.addPlaylist(displaySet.getData().getName());
        return new ResponseEntity<>(
                "Playlist " + displaySet.getData().getName() + " set to play", HttpStatus.OK);
    }

    /**
     *
     * @param displaySetId
     * @param displayTrackId
     * @return
     */
    @PostMapping(value = "/enqueue/{displaySetId}/{displayTrackId}", produces = "application/json")
    public ResponseEntity enqueue(@PathVariable("displaySetId") @OnlyNumbers String displaySetId, @PathVariable("displayTrackId") @OnlyNumbers String displayTrackId) {
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(displaySetId);
        if (displaySet.getData() == null) return new ResponseEntity<>(
                "DisplaySet " + displaySetId + " not found", HttpStatus.NOT_FOUND);

        ResponseData<DisplayTrackDto> displayTrack = displayTrackService.getDisplayTrackById(displayTrackId);
        if (displayTrack.getData() == null) return new ResponseEntity<>(
                "DisplayTrack " + displayTrackId + " not found", HttpStatus.NOT_FOUND);

        controlService.enqueueVideo(displaySet.getData().getId(), displayTrack.getData().getId(), videofileService.getAllVideoFilesByDisplayTrackId(displayTrackId, true));
        return new ResponseEntity<>(
                "Track " + displaySet.getData().getName() + " enqueued", HttpStatus.OK);
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    @PostMapping(value = "/enqueueplaylist/{displaySetId}", produces = "application/json")
    public ResponseEntity enqueuePlaylist(@PathVariable("displaySetId") @OnlyNumbers String displaySetId) {
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(displaySetId);
        if (displaySet.getData() == null) return new ResponseEntity<>("Playlist not found", HttpStatus.NOT_FOUND);
        controlService.enqueuePlaylist(displaySet.getData().getName());
        return new ResponseEntity<>(
                "Playlist " + displaySet.getData().getName() + " enqueued", HttpStatus.OK);
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    @PostMapping(value = "/clearenqueueplaylist/{displaySetId}", produces = "application/json")
    public ResponseEntity clearenqueuePlaylist(@PathVariable("displaySetId") @OnlyNumbers String displaySetId) {
        ResponseData<DisplaySetDto> displaySet = displaySetService.getDisplaySetById(displaySetId);
        if (displaySet.getData() == null) return new ResponseEntity<>("Playlist not found", HttpStatus.NOT_FOUND);
        controlService.clear();
        controlService.enqueuePlaylist(displaySet.getData().getName());
        return new ResponseEntity<>(
                "Playlist " + displaySet.getData().getName() + " cleared and enqueued", HttpStatus.OK);
    }

    /**
     *
     * @param clientId
     * @return
     */
    @PostMapping(value = "/shutdown/{clientId}", produces = "application/json")
    public ResponseEntity shutdown(@PathVariable("clientId") String clientId) {
        controlService.shutdownById(clientId);
        return new ResponseEntity<>(
                "Client " + clientId + " set to shutdown", HttpStatus.OK);
    }

    /**
     *
     * @return
     */
    @GetMapping("/test")
    public List<String> get() {
        return List.of("Hello", "edited", "s");
    }
}

