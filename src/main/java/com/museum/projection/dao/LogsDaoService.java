package com.museum.projection.dao;

import com.museum.projection.model.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

/**
 * Logs DAO Service
 */
@Service
public class LogsDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public LogsDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Retreives all logs
     * @return
     */
    public List<Log> getLogs() {
        final String sql = "SELECT message, timestamp FROM logs ORDER BY timestamp DESC";
       return jdbcTemplate.query(sql, (resultSet, i) -> {
            return new Log(resultSet.getString("message"),
                    resultSet.getTimestamp("timestamp"));
        });
    }

    /**
     *
     * @param message
     * @return
     */
    public Optional<Boolean> insertLog(String message) {
        final String SQL = "INSERT INTO logs (message, timestamp) "
                + "VALUES(?,?)";
        return Optional.of(jdbcTemplate.update(SQL, message, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET")))) > 0);
    }




}