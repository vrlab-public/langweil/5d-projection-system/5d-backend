package com.museum.projection.dao;

import com.museum.projection.model.Account;
import com.museum.projection.security.ApplicationUser;

import com.museum.projection.security.ApplicationUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Application user DAO for postgres database
 */
@Repository("postgres")
public class PostgresApplicationUserDaoService implements IApplicationUserDao {

    private final PasswordEncoder passwordEncoder;
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PostgresApplicationUserDaoService(PasswordEncoder passwordEncoder, JdbcTemplate jdbcTemplate) {
        this.passwordEncoder = passwordEncoder;
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param username
     * @return
     */
    @Override
    public Optional<ApplicationUser> getActiveApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()) && applicationUser.isEnabled())
                .findFirst();
    }

    /**
     *
     * @return
     */
    @Override
    public List<Account> getAllAccount() {
        return getAccounts();
    }

    /**
     *
     * @param username
     * @param password
     * @param roles
     * @param isEnabled
     * @return
     */
    @Override
    public Optional<Boolean> insertAccount(String username, String password, String roles, boolean isEnabled) {
        final String SQL = "INSERT INTO account(username, password, roles, active, createdcet) "
                + "VALUES(?,?,?,?,?)";
        return Optional.of(jdbcTemplate.update(SQL, username, password, roles, isEnabled, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET")))) > 0);
    }

    /**
     *
     * @param id
     * @param username
     * @param password
     * @param roles
     * @param isEnabled
     * @return
     */
    @Override
    public Optional<Boolean> updateAccount(int id, String username, String password, String roles, boolean isEnabled) {
        final String SQL = "UPDATE account SET username = ?, password = ?, roles = ?, active = ? "
                + "WHERE id = ?";
        return Optional.of(jdbcTemplate.update(SQL, username, password, roles, isEnabled, id) > 0);
    }

    /**
     *
     * @param id
     * @param password
     * @return
     * @throws DuplicateKeyException
     */
    @Override
    public Optional<Boolean> updateAccountPassword(int id, String password) throws DuplicateKeyException {
        final String SQL = "UPDATE account SET password = ?"
                + "WHERE id = ?";
        return Optional.of(jdbcTemplate.update(SQL, password, id) > 0);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Optional<Account> getAccountByUserId(int id) {
        final String sql = "SELECT id, username, password, roles, createdcet, active FROM account WHERE id = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return resultSetToAccount(resultSet);
        }));
    }

    /**
     *
     * @param username
     * @return
     */
    @Override
    public Optional<Account> getAccountByUsername(String username) {
        final String sql = "SELECT id, username, password, roles, createdcet, active FROM account WHERE username = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{username}, (resultSet, i) -> {
            return resultSetToAccount(resultSet);
        }));
    }


    private List<ApplicationUser> getApplicationUsers() {
        List<ApplicationUser> applicationUsers = getAccounts().stream().filter(account -> !account.getRoles().equals("")).map(account -> new ApplicationUser(
                Arrays.stream(account.getRoles().split(",")).flatMap(role -> ApplicationUserRole.valueOf(role).getGrantedAuthorities().stream()).collect(Collectors.toSet()),
                account.getPassword(),
                account.getUsername(),
                true,
                true,
                true,
                account.getIsEnabled())).collect(Collectors.toList());
        return applicationUsers;
    }

    private List<Account> getAccounts() {
        final String sql = "SELECT id, username, password, roles, createdcet, active FROM account ORDER BY id";

        return jdbcTemplate.query(sql, (resultSet, i) -> {
            return resultSetToAccount(resultSet);
        });
    }

    private Account resultSetToAccount(ResultSet resultSet) {
        try {
            return new Account(Integer.valueOf(resultSet.getString("id")),
                    resultSet.getString("username"),
                    resultSet.getString("password"),
                    resultSet.getString("roles"),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getBoolean("active"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}