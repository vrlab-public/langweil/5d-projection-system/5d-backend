package com.museum.projection.dao;

import com.museum.projection.model.DisplayTrack;
import com.museum.projection.model.DisplayTrackToVideoFile;
import com.museum.projection.model.Videofile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 *
 */
@Service
public class DisplayTrackToVideoFileDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DisplayTrackToVideoFileDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<DisplayTrackToVideoFile> getDisplayTracksToVideoIdById(int id) {
        final String sql = "SELECT displaytracktovideofileid, displaytrackid, videofileid, createdby, createdcet, client FROM displaytracktovideofile WHERE videofileid = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return new DisplayTrackToVideoFile(resultSet.getInt("displaytracktovideofileid"),
                    resultSet.getInt("displayTrackId"),
                    resultSet.getInt("videofileid"),
                    resultSet.getInt("client"),
                    resultSet.getInt("createdBy"),
                    resultSet.getTimestamp("createdcet"));
        }));
    }

    /**
     *
     * @param videoFileId
     * @return
     */
    public List<DisplayTrack> getDisplayTracksByVideoFileId(int videoFileId) {
        final String sql = "SELECT displaytrack.displayTrackId, displaytrack.displaySetId, displaytrack.name, displaytrack.createdby, displaytrack.createdcet, displaytrack.duration, displaytrack.trackType " +
                "FROM displaytracktovideofile " +
                "INNER JOIN displaytrack ON displaytracktovideofile.displaytrackid = displaytrack.displaytrackid " +
                "WHERE displaytracktovideofile.videofileid = ?" +
                "ORDER BY displaytracktovideofile.client";
        List<DisplayTrack> displayTracks = jdbcTemplate.query(sql, new Object[]{videoFileId}, (resultSet, i) -> {
            return new DisplayTrack(Integer.valueOf(resultSet.getString("id")),
                    resultSet.getInt("displaySetId"),
                    resultSet.getString("name"),
                    resultSet.getInt("createdBy"),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getString("description"),
                    resultSet.getString("trackType"));
        });
        return displayTracks;
    }

    /**
     *
     * @param displayTrackId
     * @return
     */
    public List<Videofile> getVideoFilesByDisplayTrackId(int displayTrackId) {
        final String sql = "SELECT videofile.videoFileId, videofile.filename, videofile.createdby, videofile.createdcet, videofile.clientSynchronized, videofile.duration, videofile.fileSize, videofile.filepath, videofile.description, videofile.videoFileType ,videofile.folderId " +
                "FROM displaytracktovideofile " +
                "INNER JOIN videofile ON displaytracktovideofile.videofileid = videofile.videofileid " +
                "WHERE displaytracktovideofile.displaytrackid = ?" +
                "ORDER BY displaytracktovideofile.client";
        List<Videofile> displayTracks = jdbcTemplate.query(sql, new Object[]{displayTrackId}, (resultSet, i) -> {
            return resultSetToVideoFile(resultSet);
        });
        return displayTracks;
    }

    private Videofile resultSetToVideoFile(ResultSet resultSet) {
        try {
            return new Videofile(resultSet.getInt("videoFileId"),
                    resultSet.getString("filename"),
                    resultSet.getString("filePath"),
                    resultSet.getInt("createdBy"),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getBoolean("clientSynchronized"),
                    resultSet.getFloat("duration"),
                    resultSet.getLong("fileSize"),
                    resultSet.getString("description"),
                    resultSet.getInt("videoFileType"),
                    resultSet.getInt("folderId"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param displayTrackId
     * @param videoFileId
     * @param clientId
     * @param createdBy
     * @return
     */
    public Optional<Long> insertDisplayTrackToVideoFile(String displayTrackId, String videoFileId, String clientId, String createdBy) {
        final String SQL = "INSERT INTO displaytracktovideofile (displayTrackId, videoFileId, client, createdBy, createdcet) "
                + "VALUES(?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"displaytracktovideofileid"});
            ps.setLong(1, Long.parseLong(displayTrackId));
            ps.setLong(2, Long.parseLong(videoFileId));
            ps.setLong(3, Long.parseLong(clientId));
            ps.setLong(4, Long.parseLong(createdBy));
            ps.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET"))));
            return ps;
        }, keyHolder);
        return Optional.of((Long) Objects.requireNonNull(keyHolder.getKey()));
    }

}