package com.museum.projection.dao;

import com.museum.projection.model.DisplayTrack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * DisplayTrackDaoService
 */
@Service
public class DisplayTrackDaoService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DisplayTrackDaoService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id
     * @return
     */
    public Optional<DisplayTrack> getDisplayTrackById(int id) {
        final String sql = "SELECT id, displaySetId, name, createdby, createdcet, duration, description, trackType FROM displaytrack WHERE id = ?";
        return Optional.ofNullable(jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, i) -> {
            return new DisplayTrack(Integer.valueOf(resultSet.getString("id")),
                    Integer.valueOf(resultSet.getString("displaySetId")),
                    resultSet.getString("name"),
                    Integer.valueOf(resultSet.getString("createdBy")),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getString("description"),
                    resultSet.getString("trackType"));
        }));
    }

    /**
     *
     * @return
     */
    public List<DisplayTrack> getDisplayTracks() {
        final String sql = "SELECT id, displaySetId, name, createdby, createdcet, duration, description, trackType FROM displaytrack ORDER BY id";
        List<DisplayTrack> displayTracks = jdbcTemplate.query(sql, (resultSet, i) -> {
            return new DisplayTrack(Integer.valueOf(resultSet.getString("id")),
                    Integer.valueOf(resultSet.getString("displaySetId")),
                    resultSet.getString("name"),
                    Integer.valueOf(resultSet.getString("createdBy")),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getString("description"),
                    resultSet.getString("trackType"));
        });
        return displayTracks;
    }

    /**
     *
     * @param name
     * @param displaySetId
     * @param createdBy
     * @param duration
     * @param description
     * @param trackTypes
     * @return
     */
    public Optional<Long> insertDisplayTrack(String name, String displaySetId, int createdBy, float duration, String description, String trackTypes) {
        final String SQL = "INSERT INTO displaytrack (name, displaySetId, createdBy, createdcet, duration, description, trackType) "
                + "VALUES(?,?,?,?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(SQL, new String[]{"id"});
            ps.setString(1, name);
            ps.setLong(2, Long.parseLong(displaySetId));
            ps.setLong(3, createdBy);
            ps.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now(ZoneId.of("CET"))));
            ps.setFloat(5, duration);
            ps.setString(6, description);
            ps.setString(7, trackTypes);
            return ps;
        }, keyHolder);
        return Optional.of((Long) Objects.requireNonNull(keyHolder.getKey()));
    }

    /**
     *
     * @param displaySetId
     * @return
     */
    public List<DisplayTrack> getDisplayTrackByDisplaysetId(String displaySetId) {
        final String sql = "SELECT id, displaySetId, name, createdby, createdcet, duration, description, trackType FROM displaytrack WHERE displaysetid = ? ORDER BY id";
        List<DisplayTrack> displayTracks = jdbcTemplate.query(sql, new Object[]{Long.parseLong(displaySetId)}, (resultSet, i) -> {
            return new DisplayTrack(Integer.valueOf(resultSet.getString("id")),
                    Integer.valueOf(resultSet.getString("displaySetId")),
                    resultSet.getString("name"),
                    Integer.valueOf(resultSet.getString("createdBy")),
                    resultSet.getTimestamp("createdcet"),
                    resultSet.getFloat("duration"),
                    resultSet.getString("description"),
                    resultSet.getString("trackType"));
        });
        return displayTracks;
    }
}