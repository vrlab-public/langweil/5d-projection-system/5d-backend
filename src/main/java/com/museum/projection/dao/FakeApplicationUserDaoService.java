package com.museum.projection.dao;

import com.google.common.collect.Lists;
import com.museum.projection.model.Account;
import com.museum.projection.security.ApplicationUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static com.museum.projection.security.ApplicationUserRole.*;

/**
 * Fake User DAO when authentication is disabled
 */
@Repository("fake")
public class FakeApplicationUserDaoService implements IApplicationUserDao {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public FakeApplicationUserDaoService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<ApplicationUser> getActiveApplicationUserByUsername(String username) {
        return getApplicationUsers()
                .stream()
                .filter(applicationUser -> username.equals(applicationUser.getUsername()))
                .findFirst();
    }

    @Override
    public List<Account> getAllAccount() {
        return List.of();
    }

    @Override
    public Optional<Boolean> insertAccount(String username, String password, String roles, boolean isEnabled) {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> updateAccount(int id, String username, String encode, String s, boolean active) {
        return Optional.empty();
    }

    @Override
    public Optional<Boolean> updateAccountPassword(int id, String password) {
        return null;
    }

    @Override
    public Optional<Account> getAccountByUserId(int id) {
        return null;
    }

    @Override
    public Optional<Account> getAccountByUsername(String username) {
        return Optional.empty();
    }


    private List<ApplicationUser> getApplicationUsers() {
        List<ApplicationUser> applicationUsers = Lists.newArrayList(
                new ApplicationUser(
                        PRESENTATOR.getGrantedAuthorities(),
                        passwordEncoder.encode("password"),
                        "annasmith",
                        true,
                        true,
                        true,
                        true
                ),
                new ApplicationUser(
                        ADMIN.getGrantedAuthorities(),
                        passwordEncoder.encode("nimda"),
                        "admin",
                        true,
                        true,
                        true,
                        true
                ),
                new ApplicationUser(
                        PRESENTATOR.getGrantedAuthorities(),
                        passwordEncoder.encode("tom"),
                        "tom",
                        true,
                        true,
                        true,
                        true
                )
        );

        return applicationUsers;
    }

}
