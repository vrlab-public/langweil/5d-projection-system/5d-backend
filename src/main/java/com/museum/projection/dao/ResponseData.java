package com.museum.projection.dao;

import java.util.Optional;

/**
 * Data object for bundled returning of data, message and field
 * @param <T>
 */
public class ResponseData<T> {

    private String message;

    private String field;

    private T data;

    public ResponseData(T data, String message, String field) {
        this.message = message;
        this.data = data;
        this.field = field;
    }

    public ResponseData(T data, String message) {
        this.message = message;
        this.data = data;
    }

    public ResponseData(T data) {
        this.data = data;
    }

    public ResponseData() {
    }

    public T getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getField() {
        return field;
    }

}
