package com.museum.projection.dao;

import com.museum.projection.model.Account;
import com.museum.projection.security.ApplicationUser;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Interface for Application user DAO
 * Integration with Spring Security
 */
public interface IApplicationUserDao {

    Optional<ApplicationUser> getActiveApplicationUserByUsername(String username);

    List<Account> getAllAccount();

    Optional<Boolean> insertAccount(String username, String password, String roles, boolean isEnabled) throws DuplicateKeyException;

    Optional<Boolean> updateAccount(int id, String username, String password, String roles, boolean active) throws DuplicateKeyException, EmptyResultDataAccessException;

    Optional<Boolean> updateAccountPassword(int id, String password) throws EmptyResultDataAccessException;

    Optional<Account> getAccountByUserId(int id) throws EmptyResultDataAccessException;

    Optional<Account> getAccountByUsername(String username);


}
