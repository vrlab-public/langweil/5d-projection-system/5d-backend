package com.museum.projection;


import com.museum.projection.clients.DummyClient;
import com.museum.projection.clients.RemoteClient;
import com.museum.projection.config.CustomConfig;
import com.museum.projection.dao.ResponseData;
import com.museum.projection.dto.other.PlaylistTrack;
import com.museum.projection.service.ControlService;
import com.museum.projection.service.PlaylistService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class PlaylistServiceTests {

    final int CLIENTS = 2;
    final String VIDEO_PATH = "/videopath/";

    private PlaylistService playlistService;

    @Before
    public void init() {
        CustomConfig config = new CustomConfig();
        config.setVideoFolderPath(VIDEO_PATH);
        playlistService = new PlaylistService(config);
    }

    @Test
    public void testToServerFileName()
            throws Exception {
        String folderPath = "/home/user/";
        String displaySetId = "1";
        String displayTrackId = "2";
        String fileName = "file.mp4";
        assertThat(PlaylistService.toServerFileName(folderPath, displaySetId, displayTrackId, fileName)).isEqualTo("/home/user/1/2/1-2-file.mp4");
    }

    @Test
    public void testGetPlaylist()
            throws Exception {
        String playlistName = "My Test playlist";
        assertThat(PlaylistService.getPlaylistName(playlistName)).isEqualTo("My_Test_playlist");
    }

    @Test
    public void testGeneratePlaylist()
            throws Exception {
        String displaySetId = "1";
        String displaySetName = "testSet";
        ArrayList<PlaylistTrack>[] playlistsData = new ArrayList[CLIENTS];
        for (int i = 0; i < CLIENTS; i++) {
            playlistsData[i] = new ArrayList<PlaylistTrack>();
            playlistsData[i].add(new PlaylistTrack("/path/" + i, "filename" + i, (float) i, "displayTrack" + i, String.valueOf(i)));
        }

        ResponseData<String[]> responseData = playlistService.generatePlaylists(displaySetName, displaySetId, playlistsData);
        System.out.println(Arrays.toString(responseData.getData()));
        assertThat(responseData.getData()).isNotEmpty();
        assertThat(responseData.getData().length).isEqualTo(CLIENTS);
        for (int i = 0; i < CLIENTS; i++) {
            assertThat(responseData.getData()[i]).isEqualTo("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<playlist xmlns=\"http://xspf.org/ns/0/\" xmlns:vlc=\"http://www.videolan.org/vlc/playlist/ns/0/\" version=\"1\"><title>testSet</title><trackList><track><location>file:///videopath/" + displaySetId + "/" + i + "/" + displaySetId + "-" + i + "-filename" + i + "</location><duration>" + (i * 1000) + "</duration><extension application=\"http://www.videolan.org/vlc/playlist/0\"><vlc:id>0</vlc:id><vlc:option>recursive=collapse</vlc:option></extension></track></trackList><extension application=\"http://www.videolan.org/vlc/playlist/0\"><vlc:item tid=\"0\"/></extension></playlist>");

        }
    }

}
